/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        image_blue: {
          100: "rgb(39, 49, 54)",
          200: "rgb(90, 123, 120)",
          300: "rgb(52, 66, 69)",
          400: "rgb(26, 28, 35)"
        }
        ,
        "custom-black": {
          100: "#222423"
        }
      },
      fontFamily: {
        kalam: ["Kalam", "cursive"]
      }
    },
  },
  plugins: [],
}

